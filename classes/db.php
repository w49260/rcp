<?php

/**
 * Klasa db - do zarządzania bazą danych MySQL
 */
class db {
	// deklarujemy zmienne naszej klasy
	// na początek zmienny prywatne, niedostępne poza naszą klasą
	// host bazy danych
	private $db_host = DATABASE_SERWER;
	// nazwa bazy danych
	private $db_name = DATABASE;
	// nazwa użytkownika z uprawnieniami odczytu i zapisu
	private $db_user = DATABASE_USERNAME;
	// hasło użytkownika
	private $db_passw = DATABASE_PASSWORD;
	
	// zmienne publiczne - dostępne poza klasą
	public $connection;
	// połączenie
	public $error;
	// ew. błąd zwrócony przez bazę danych

	public function __construct() {

		if (@$connection = mysql_connect($this -> db_host, $this -> db_user, $this -> db_passw)) {
			// @ - operator kontroli błedów - nie zwróci ewentualnych błedów
			if (mysql_select_db($this -> db_name, $connection)) {
				$this -> connection = $connection;
				mysql_set_charset('utf8', $this -> connection);
				return true;
			} else {
				throw new Exception(mysql_error());
				return false;
			}
		} else {
			throw new Exception(mysql_error());
			return false;
		}
	}

	// eof connect()

	public function select($sql) {

		if ($this -> connection) {
			mysql_set_charset('utf8', $this -> connection);
			if (isset($sql) && $sql != '') {
				if ($result = mysql_query($sql)) {
					return $result;
				} else {
					throw new Exception(mysql_error());
					return false;
				}
			} else {
				throw new Exception('Błąd zapytania SQL');
				return false;
			}
		} else {
			throw new Exception('Brak połączenia z bazą danych');
			return false;
		}
	}

	// eof select()

	public function query($sql) {

		if (isset($sql) && $sql != '') {
			if ($this -> connection) {
				mysql_set_charset('utf8', $this -> connection);
				if (mysql_query($sql)) {
					return true;
				} else {
					throw new Exception(mysql_error());
					return false;
				}
			} else {
				throw new Exception('Brak połączenia z bazą danych');
				return false;
			}
		} else {
			throw new Exception('Błąd zapytania SQL');
			return false;
		}
	}

	// eof query()

	public function __destruct() {
		if ($this -> connection) {
			@mysql_close();
		}
	}

	// eof close()
}
?>
