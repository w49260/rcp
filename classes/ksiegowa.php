<?php

/**
 * Klasa ksiegowa - do zarządzania funkcjami dla ksiegowej
 * Księgowa dziedziczy po pracowniku
 */
class ksiegowa extends pracownik {

	private $db;

	/**
	 * Funcja zwraca tabelkę z listą pracowników
	 * parametr $czy_pokazywac_stawki - jeśli TRUE to obok nazwy pracownika pojawi się stawka godzinowa i stawka za nadgodziny
	 * Pamiętać, aby wcześniej dać listę select!
	 */
	public function getListPracownicy($czy_pokazywac_stawki) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// zapisujemy wszystkie dane do zmiennej
		$zapytanie = $this -> db -> select("SELECT login, stawka_godzinowa, stawka_nadgodziny FROM pracownicy WHERE rodzaj_konta_id!=1");
		// rodzaj_konta_id!=1 - ponieważ księgowa nie może zmienić szefowi pensji (a szef ma rodzaj konta 1)

		while ($dane = mysql_fetch_array($zapytanie)) {
			// sprawdzamy czy pokazywać stawki
			if ($czy_pokazywac_stawki) {
				echo '<option>' . $dane["login"] . '	(godzinowa: ' . $dane["stawka_godzinowa"] . ' zł | nadgodziny: ' . $dane["stawka_nadgodziny"] . ' zł)' . '</option>';
			} else {
				echo '<option>' . $dane["login"] . '</option>';
			}
		}
	}

	/**
	 * Funcja zwraca tabelkę z listą nadgodzin
	 * parametr $login - login pracownika
	 * parametr rok - to rok w ktorym ma byc lista nadgodzin - domyslnie aktualny rok
	 * parametr miesiac - to miesiac w ktorym ma byc lista nadgodzin - domyslnie aktualny miesiac
	 */
	public function getListNadgodzin($login, $rok = NULL, $miesiac = NULL) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// Sprawdzamy czy parametry rok i miesiac sa puste, jesli tak to domyslnie ustawiamy na aktualny rok i miesiac
		if (is_null($rok)) {
			$rok = date("Y");
		}
		if (is_null($miesiac)) {
			$miesiac = date("m");
		}

		// wyświetlamy kod tabelki
		echo '<div class="tabelaobecnosci">
		<table class="table" style=" display: inline;">
				<caption style="font-size: 16px; color: #3A87AD;">
					Lista nadgodzin (' . $rok . '-' . $miesiac . ')
				</caption>
				<thead>
					<tr>
						<th>Lp</th>
						<th>Data</th>
						<th>Czas</th>
						<th>Dodatkowe informacje</th>
					</tr>
				</thead>
				<tbody>';

		// zapisujemy wszystkie dane do zmiennej
		$zapytanie = $this -> db -> select("SELECT data, czas, dodatkowe_informacje FROM nadgodziny WHERE login='$login' and month(data)=month('$rok-$miesiac-01')");

		$lp = 0;
		$wejscie = null;
		// pętlą wyświetlamy wszystkie dane
		while ($dane = mysql_fetch_array($zapytanie)) {
			$lp++;

			// Usuwamy z daty niepotrzebną godzinę
			$data = str_replace(' 00:00:00', '', $dane["data"]);

			echo '<tr class="success">';
			echo '<td>' . $lp . '</td>';
			echo '<td>' . $data . '</td>';
			echo '<td>' . $dane["czas"] . '</td>';
			echo '<td>' . $dane["dodatkowe_informacje"] . '</td>';
			echo '<td></td>';

		}
		// wyświetlamy końcowy kod tabelki
		echo '</tr>
			</tbody>
			</table></div>';
	}

	/**
	 * Funkcja służąca do zapisywania pensji
	 * parametr: $login - login pracownika
	 * parametr $godzinowa - stawka godzinowa w formacie np. 10.50
	 * parametr $nadgodziny - stawka za nadgodziny w formacie np. 10.50
	 */
	public function savePensja($login, $godzinowa, $nadgodziny) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = $this -> db -> query("UPDATE `pracownicy` SET `stawka_godzinowa` = '$godzinowa', `stawka_nadgodziny` = '$nadgodziny' WHERE `login` = '$login';");
		// sprawdzamy czy zapytanie wykonało się dobrze
		if ($zapytanie) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Funkcja do zwracania kodu pracownika
	 * parametr $login - login pracownika
	 */
	public function getKodPracownika($login) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT kod_pracownika FROM pracownicy WHERE login='$login'"));

		$kodpracownika = $zapytanie["kod_pracownika"];
		return $kodpracownika;
	}

	/**
	 * Funkcja zwraca stawkę godzinową pracownika
	 * parametr $login - login pracownika
	 */
	public function getStawkaGodzinowa($login) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT stawka_godzinowa FROM pracownicy WHERE login='$login'"));

		$stawka = $zapytanie["stawka_godzinowa"];
		return $stawka;
	}

	/**
	 * Funkcja zwraca stawkę nadgodzinową pracownika
	 * parametr $login - login pracownika
	 */
	public function getStawkaNadGodzinowa($login) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT stawka_nadgodziny FROM pracownicy WHERE login='$login'"));

		$stawka = $zapytanie["stawka_nadgodziny"];
		return $stawka;
	}

	/**
	 * Funkcja służąca do wyliczania pensji danego pracownika
	 * parametr: $login - login pracownika
	 * parametr: $miesiac - miesiąc wyliczanej pensji
	 * parametr: $rok - rok wyliczanej pensji
	 */
	public function wyliczPensje($login, $miesiac, $rok) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// pobieramy kod pracownika
		$kodpracownika = $this -> getKodPracownika($login);

		// najpierw pobieramy przepracowany czas pracownika w pracy
		$zapytanie = $this -> db -> select("SELECT data, czy_wyjscie FROM odklikania WHERE kod_pracownika='$kodpracownika' and month(data)=month('$rok-$miesiac-01')");

		$ilosc_dni_w_pracy = 0;
		$lacznie_przepracowany_czas = 0;

		$wejscie = null;
		while ($dane = mysql_fetch_array($zapytanie)) {
			$ilosc_dni_w_pracy++;
			// Sprawdzamy czy użytkownik wyszedł - dopiero wtedy liczymi liczbę godzin
			if ($dane["czy_wyjscie"]) {
				// użytkownik wyszedł więc dodajemy przepracowany czas
				$lacznie_przepracowany_czas += (strtotime($dane["data"]) - strtotime($wejscie)) - 3600;
				// -3600 ponieważ php dodaje zawsze + 1 godz

			} else {
				// użytkownik wszedł, więc zapamiętujemy datę wejścia!
				$wejscie = $dane["data"];
			}
		}

		$ilosc_przepracowanych_w_pracy_godzin = date("G", $lacznie_przepracowany_czas);

		echo '<p class="center">Pracownik przepracował w pracy łącznie: ' . $lacznie_przepracowany_czas . ' sekund, czyli ' . $ilosc_przepracowanych_w_pracy_godzin . ' godz. ' . date("i", $lacznie_przepracowany_czas) . ' min. ' . date("s", $lacznie_przepracowany_czas) . ' sec.</p>';

		// Liczymy teraz pensję pracownika przepracowaną w pracy - ilość godzin * stawka godzinowa
		$stawka_godzinowa = $this -> getStawkaGodzinowa($login);
		$pensja_godzinowa = $ilosc_przepracowanych_w_pracy_godzin * $stawka_godzinowa;

		echo '<p class="center">Pracownik ' . $login . " zarabia na godzinę: " . $stawka_godzinowa . ' zł, tak więc w pracy zarobił: <strong>' . $pensja_godzinowa . 'zł</strong>.</p>';

		// Pobieramy nadgodziny
		$stawka_nadgodzinowa = $this -> getStawkaNadGodzinowa($login);

		// Pobieramy nadgodziny z bazy
		$zapytanie = $this -> db -> select("SELECT czas FROM nadgodziny WHERE login='$login' and month(data)=month('$rok-$miesiac-01')");
		$lacznie_nadgodzin = 0;
		while ($dane = mysql_fetch_array($zapytanie)) {
			$lacznie_nadgodzin += $dane["czas"];
		}
		echo '<p class="center">Pracownik przepracował w ramach nadgodzin: ' . $lacznie_nadgodzin . ' godz.</p>';

		// Liczymy teraz pensję pracownika przepracowaną w ramach nadgodzin - ilość nadgodzin * stawka nadgodzinowa
		$pensja_nadgodziny = $lacznie_nadgodzin * $stawka_nadgodzinowa;

		echo '<p class="center">Pracownik w ramach nadgodzin zarabia na godzinę: ' . $stawka_nadgodzinowa . ' zł, tak więc zarobił dodatkowo: <strong>' . $pensja_nadgodziny . 'zł</strong>.</p>';

		// Wyliczamy sumę pensji

		$suma_pensji = $pensja_godzinowa + $pensja_nadgodziny;

		echo '<h2><p class="center">W sumie pracownik w miesiącu: ' . $miesiac . '-' . $rok . ' zarobił: <span style="color: red;">' . $suma_pensji . 'zł</span></p></h2>';

	}

}
?>