<?php

/**
 * Klasa szef - do zarządzania funkcjami dla szefa
 */
class szef extends ksiegowa {

	private $db;

	/**
	 * Funkcja służąca do dodawania nowych pracowników
	 * parametr $imie - imie nowego pracownika
	 * parametr $nazwisko - nazwisko nowego pracownika
	 * parametr $haslo - haslo nowego pracownika
	 * parametr $stawka_godzinowa - stawka godzinowa nowego pracownika
	 * parametr $stawka_nadgodzin - stawka nadgodzin nowego pracownika
	 * parametr $rodzaj_konta - rodzaj konta (Szef, Księgowa, Pracownik)
	 */
	public function saveNowyPracownik($imie, $nazwisko, $haslo, $stawka_godzinowa, $stawka_nadgodzin, $rodzaj_konta) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// Sprawdzamy czy taki pracownik już istnieje
		if ($this -> sprawdzCzyIstniejePracownik($imie, $nazwisko)) {
			// pracownik istnieje więć błąd
			echo '<p class="center">Taki pracownik już istnieje w systemie. Sprawdź czy nie dodajesz go drugi raz.</p>';

		} else {

			// Tworzymy login - 1 litera imienia + nazwisko
			$array_imie = str_split($imie);
			$pierwsza_litera_imienia = $array_imie[0];
			// zamiana na małe litery 1 litery imienia + nazwisko
			$login = strtolower($pierwsza_litera_imienia) . strtolower($nazwisko);

			//md5 z hasła
			$haslo = md5($haslo);

			//Tworzymy kod pracownika - +1 do ostatniego kodu
			$kod_pracownika = $this -> getLastKodPracownika() + 1;

			// Pobieramy ID rodzaju konta
			$id_rodzaj_konta = $this -> getIDRodzajKonta($rodzaj_konta);

			$zapytanie = $this -> db -> query("INSERT INTO `pracownicy` (`id`, `rodzaj_konta_id`, `login`, `haslo`, `kod_pracownika`, `imie`, `nazwisko`, `stawka_godzinowa`, `stawka_nadgodziny`)
				VALUES (NULL, '$id_rodzaj_konta', '$login', '$haslo', '$kod_pracownika', '$imie', '$nazwisko', '$stawka_godzinowa', '$stawka_nadgodzin');");
			// sprawdzamy czy zapytanie wykonało się dobrze
			if ($zapytanie) {
				echo '<p class="center">Nowy pracownik został dodany!<br>Jego login to: <strong>' . $login . '</strong></p>';

				// Wyświetlamy kod QR
				echo '<h2><p class="center">Kod QR do wydrukowania dla pracownika: ' . $login . '</p></h2>';
				echo $this -> getQRPracownika($kod_pracownika);

			} else {
				echo '<p class="center">Wystąpił błąd. Skontaktuj się z autorem systemu</p>';
			}
		}
	}

	/**
	 * Funkcja służąca do usuwania pracownika
	 * paramater $login - login pracownika
	 */
	public function usunPracownika($login) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = $this -> db -> select("DELETE FROM `pracownicy` WHERE login='$login'5");
		// sprawdzamy czy zapytanie wykonało się dobrze
		if ($zapytanie) {
			echo '<p class="center">Pracownik: <strong>' . $login . '</strong> został usunięty!</p>';

		} else {
			echo '<p class="center">Wystąpił błąd. Skontaktuj się z autorem systemu</p>';
		}

	}

	/**
	 * Funkcja służąca do wyświetlenia kodu QR pracownika
	 * parametr $kod_pracownika - kod pracownika
	 */
	public function getQRPracownika($kod_pracownika) {
		include ('qrcode/qrlib.php');
		$tempDir = 'qrcode/temp/';
		QRcode::png($kod_pracownika, $tempDir . 'kod.png', QR_ECLEVEL_L, 4);
		echo '<p style="text-align: center;"><img src="' . $tempDir . 'kod.png" /></p>';
	}

	/**
	 * Funkcja zwraca ID rodzaju konta
	 * parametr $nazwa to nazwa rodzaju konta (np. Szef)
	 */
	public function getIDRodzajKonta($nazwa) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT id FROM rodzaj_konta WHERE nazwa='$nazwa'"));

		$id = $zapytanie["id"];
		return $id;
	}

	/**
	 * Funkcja zwraca ostatni użyty kod_pracownika
	 */
	private function getLastKodPracownika() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT kod_pracownika FROM pracownicy ORDER BY `kod_pracownika` DESC LIMIT 1"));

		$kod_pracownika = $zapytanie["kod_pracownika"];
		return $kod_pracownika;
	}

	/**
	 * Funkcja służąca do sprawdzania imienia i nazwiska pracownika - czy już takiego pracownika nie ma
	 * zwraca true jeśli pracownik już jest, false jeśli nie ma
	 */
	private function sprawdzCzyIstniejePracownik($imie, $nazwisko) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = $this -> db -> select("SELECT * FROM pracownicy WHERE imie='$imie' and nazwisko='$nazwisko'");

		if (mysql_num_rows($zapytanie) == 1) {
			// pracownik jest więc true
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Funkcja pokazuje listę wszystkich pracowników wraz z ich statusami (czy są w pracy)
	 */
	public function pokazListePracownikow() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// sprawdzamy czy w ogóle są pracownicy
		$zapytanie = $this -> db -> select("SELECT * FROM pracownicy");
		if (mysql_num_rows($zapytanie) <> 0) {
			// są pracownicy
			echo '<div class="tabelaobecnosci" style="width: 800px;">
			<table class="table" style="margin-bottom: 0">
				<caption style="font-size: 16px; color: #3A87AD;">
					Lista pracowników <br><a href="panel_szef.php" class="btn btn-primary">Odśwież</a>
				</caption>
				<thead>
					<tr>
						<th>Imię</th>
						<th>Nazwisko</th>
						<th>Status w pracy</th>
					</tr>
				</thead>
				<tbody>';
			// pętla po wszystkich pracownikach
			while ($dane = mysql_fetch_array($zapytanie)) {
				// sprawdzamy czy pracownik jest w pracy (tzn. czy ostatni status wyjście != 1) - jeśli tak to wyświetlimy inny kolor

				$kod_pracownika = $dane['kod_pracownika'];
				$pracownik = mysql_fetch_array($this -> db -> select("SELECT data, czy_wyjscie FROM odklikania WHERE kod_pracownika='$kod_pracownika' ORDER BY data DESC LIMIT 1"));
				if ($pracownik["czy_wyjscie"]) {
					// pracownik wyszedł
					echo '<tr class="danger" style="text-align: left;">';
				} else {
					// sprawdzamy czy pracownik był kiedyś (czy się odklikał)
					if (empty($pracownik['data'])) {
						// pracownika nigdy nie było
						echo '<tr class="danger" style="text-align: left;">';
					} else {
						// pracownik był
						echo '<tr class="success" style="text-align: left;">';
					}
				}

				echo '<td>' . $dane['imie'] . '</td>';
				echo '<td>' . $dane['nazwisko'] . '</td>';

				// sprawdzamy czy pracownik był kiedyś (czy się odklikał)
				if (empty($pracownik['data'])) {
					echo '<td>Pracownik nigdy nie był obecny!</td>';
				} else {
					// sprawdzamy czy pracownik wyszedł
					if ($pracownik["czy_wyjscie"]) {
						// pracownik wyszedł
						echo '<td>Pracownik nie obecny (od: ' . $pracownik['data'] . ')</td>';
					} else {

						echo '<td>Pracownik obecny (od: ' . $pracownik['data'] . ')</td>';
					}
				}
				echo '</tr>';

			}
			echo '</tbody>
			</table></div>';

		}
	}

}
?>
