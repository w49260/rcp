<?php

/**
 * Klasa odklikaj - do odklikiwania pracowników
 */
class odklikiwanie {

	private $kod_pracownika;
	private $db;

	/**
	 * Konstruktor klasy odklikiwanie
	 */
	public function __construct($kod_pracownika) {
		$this -> kod_pracownika = $kod_pracownika;

		// wywołujemy w konstruktorze połączenie z bazą
		$this -> db = new db();
	}

	/**
	 * Funkcja do sprawdzania czy dany pracownik istnieje
	 */
	public function czyPracownikIstnieje() {
		$czy_pracownik = $this -> db -> select("SELECT * FROM pracownicy WHERE kod_pracownika='$this->kod_pracownika'");
		if (mysql_num_rows($czy_pracownik) == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Funkcja do sprawdzania czy dany pracownik kiedyś się odklikiwał
	 */
	public function czyPracownikSieOdklikiwal() {
		$ostatnie_odklikanie = $this -> db -> select("SELECT * FROM odklikania WHERE kod_pracownika='$this->kod_pracownika'");
		if (mysql_num_rows($ostatnie_odklikanie) == 0) {
			// ten pracownik się nigdy nie odklikiwał
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Funkcja do odklikiwania - dodawania od bazy wpisu o odklikaniu
	 * parametr $czy_wyjscie = true w przypadku jeśli WYCHODZI z pracy, false jeśli PRZYCHODZI
	 */
	public function odklikajPrzyjscie($czy_wyjscie) {
		if ($czy_wyjscie) {
			// Pracownik wychodzi a wiec oznaczamy wyjście
			$this -> db -> query("INSERT INTO odklikania VALUES('',NOW(),$this->kod_pracownika,TRUE)");
			echo "Wyszedles z pracy";
		} else {
			// Pracownik przychodzi a więc oznaczamy przyjście
			$this -> db -> query("INSERT INTO odklikania VALUES('',NOW(),$this->kod_pracownika,FALSE)");
			echo "Przyszedles do pracy";
		}
	}

	/**
	 * Funkcja zwracająca ile sekund mineło od ostatniego odklikiwania
	 */
	public function ileMineloOdOstatniegoOdklikiwania() {
		$ostatnie_odklikanie_dane = mysql_fetch_array($this -> db -> select("SELECT * FROM odklikania WHERE kod_pracownika='$this->kod_pracownika' ORDER BY id DESC LIMIT 1"));
		$roznica_w_odklikaniu = StrToTime(Date('Y-m-d H:i:s')) - StrToTime($ostatnie_odklikanie_dane["data"]);
		return $roznica_w_odklikaniu;
	}

	/**
	 * Funkcja zwraca czy pracownik ostatnio wychodził z pracy
	 * True jeśli tak, False jeśli nie
	 */
	public function czyWyjscie() {
		$wychodzil = mysql_fetch_array($this -> db -> select("SELECT * FROM odklikania WHERE kod_pracownika='$this->kod_pracownika' ORDER BY id DESC LIMIT 1"));
		return $wychodzil["czy_wyjscie"];
	}

}
?>
