<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if ($_SESSION['rodzaj_konta'] == "Szef") {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$szef = new szef();

		// Sprawdzamy czy wysłano zapytanie o dodanie pracownika
		if ((isset($_POST['imie'])) && (isset($_POST['nazwisko'])) && (isset($_POST['haslo'])) && (isset($_POST['godzinowa'])) && (isset($_POST['nadgodziny'])) && (isset($_POST['rodzaj_konta']))) {

			$imie = htmlspecialchars($_POST["imie"]);
			$nazwisko = htmlspecialchars($_POST["nazwisko"]);
			$haslo = htmlspecialchars($_POST["haslo"]);
			$godzinowa = htmlspecialchars($_POST["godzinowa"]);
			$nadgodziny = htmlspecialchars($_POST["nadgodziny"]);
			$rodzaj_konta = htmlspecialchars($_POST["rodzaj_konta"]);

			echo $szef -> saveNowyPracownik($imie, $nazwisko, $haslo, $godzinowa, $nadgodziny, $rodzaj_konta);

		} else {
			// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający dodanie pracownika

			echo '<form action="panel_szef_dodaj_pracownika.php" method="post">
			<div class="nadgodziny">
			<p>Imię: <input type="text" name="imie" class="input100"></p>
			<p>Nazwisko: <input type="text" name="nazwisko" class="input100"></p>
			<p>Hasło: <input type="password" name="haslo" class="input100"></p>
			<p>Stawka godzinowa (podawana z dwoma miejscami po kropce, np. 8.50 - to 8 zł. 50 gr.): <input type="text" name="godzinowa" class="input100"></p>
   			<p>Stawka nadgodzin (podawana z dwoma miejscami po kropce, np. 8.50 - to 8 zł. 50 gr.): <input type="text" name="nadgodziny" class="input100"></p>
   			Rodzaj konta:
			<select name="rodzaj_konta">
			<option>Pracownik</option>
			<option>Księgowa</option>
			<option>Szef</option>
			</select>
			<br><br>
			<input type="submit" value="Dodaj pracownika" class="btn btn-success btn-lg btn-block" />
			</form>';

		}

		// Wyświetlamy przycisk do wrócenia do poprzedniej strony
		echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>