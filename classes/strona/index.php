<?php include ('header.php'); ?>

<form class="form-signin" action="login.php" method="post">
	<h2 class="form-signin-heading">Logowanie do systemu<br>Rejestru Czasu Pracy</h2>
	<input type="hidden" name="send" value="TRUE" />
	<input type="text" class="input-block-level" name="login" placeholder="Login">
	<br>
	<input type="password" class="input-block-level" name="haslo" placeholder="Hasło">
	<br>
	<button class="btn btn-large btn-primary" type="submit">
		Zaloguj się
	</button>
</form>

<?php include ('footer.php'); ?>