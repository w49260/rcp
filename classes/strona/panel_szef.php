<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if ($_SESSION['rodzaj_konta'] == "Szef") {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$pracownik = new pracownik();
		$pracownik -> setLogin($_SESSION['login']);

		echo "<p class=\"center\"><strong>Witaj szefie: " . $pracownik -> getImie() . " " . $pracownik -> getNazwisko() . "</strong></p>";

		// Wyświetlamy szefowi listę wszystkich pracowników w raz z ich statusem w pracy
		$szef = new szef();
		echo $szef -> pokazListePracownikow();

		// Wyświetlamy przycisk do dodania pracownika
		echo '<a href="panel_szef_dodaj_pracownika.php" type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 13px;">Dodaj pracownika</a>';

		// Wyświetlamy przycisk do usunięcia pracowina
		echo '<a href="panel_szef_usun_pracownika.php" type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 13px;">Usuń pracownika</a>';

		// Wyświetlamy przycisk do wygenerowania kodu QR pracownikowi
		echo '<a href="panel_szef_generuj_qr_pracownika.php" type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 13px;">Generuj kod QR pracownikowi</a>';

		// Wyświetlamy przycisk do ustawiania kwoty pęsji pracowników
		echo '<a href="panel_ksiegowosc_ustaw_pensje.php" type="button" class="btn btn-warning btn-lg btn-block" style="font-size: 13px;">Ustaw pensję pracownikowi</a>';

		// Wyświetlamy przycisk do wyliczania pensji miesięcznej
		echo '<a href="panel_ksiegowosc_wylicz_pensje.php" type="button" class="btn btn-warning btn-lg btn-block" style="font-size: 13px;">Wylicz pensję pracownikowi</a>';

		// Wyświetlamy przycisk do generowania raportu obecności pracownika
		echo '<a href="panel_ksiegowosc_generuj_raport_obecnosci.php" type="button" class="btn btn-warning btn-lg btn-block" style="font-size: 13px;">Generuj raport obecności pracownika</a>';

		// Wyświetlamy przycisk do generowania raportu nadgodzin pracownika
		echo '<a href="panel_ksiegowosc_generuj_raport_nadgodzin.php" type="button" class="btn btn-warning btn-lg btn-block" style="font-size: 13px;">Generuj raport nadgodzin pracownika</a>';

		// Wyświetlamy przycisk do dodawania nadgodzin
		echo '<a href="panel_pracownik_nadgodziny.php" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Dodaj nadgodziny</a>';

		// Wyświetlamy przycisk do zmiany hasła
		echo '<a href="panel_pracownik_zmien_haslo.php" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Zmień hasło</a>';

		// Wyświetlamy przycisk do wylogowania
		echo '<a href="wyloguj.php" type="button" class="btn btn-danger btn-lg btn-block" style="font-size: 13px;">Wyloguj</a>';

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>