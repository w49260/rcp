<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if ($_SESSION['rodzaj_konta'] == "Pracownik") {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$pracownik = new pracownik();
		$pracownik -> setLogin($_SESSION['login']);

		echo "<p class=\"center\"><strong>Witaj pracowniku: " . $pracownik -> getImie() . " " . $pracownik -> getNazwisko() . "</strong></p>";

		// Wyświetlamy przycisk do dodawania nadgodzin
		echo '<a href="panel_pracownik_nadgodziny.php" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Dodaj nadgodziny</a>';

		// Wyświetlamy przycisk do zmiany hasła
		echo '<a href="panel_pracownik_zmien_haslo.php" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Zmień hasło</a>';
		
		// Wyświetlamy przycisk do wylogowania
		echo '<a href="wyloguj.php" type="button" class="btn btn-danger btn-lg btn-block" style="font-size: 13px;">Wyloguj</a>';

		// Wyświetlamy formularz do zmiany daty listy obecnośći
		echo '<div class="formularzobecnosci">
		Podaj miesiąc i rok aby wyświetlić listy:
		<br>
		<br>
		<form action="panel_pracownik.php" method="get">
	<fieldset>
		<select name="miesiac">
			<option>01</option>
			<option>02</option>
			<option>03</option>
			<option>04</option>
			<option>05</option>
			<option>06</option>
			<option>07</option>
			<option>08</option>
			<option>09</option>
			<option>10</option>
			<option>11</option>
			<option>12</option>
   		</select>
   		<select name="rok">
			<option>2014</option>
			<option>2015</option>
			<option>2016</option>
   		</select>
   		<br>
   		<br>
   		<input type="submit" value="Pokaż miesiąc" class="btn btn-success" />
   </fieldset>
   </form>
   </div>';

		// sprawdzamy czy istnieją zmienne miesiąc i rok
		if ((isset($_GET['miesiac'])) && (isset($_GET['rok']))) {
			// istnieją, więc wyświetlamy listę obecności z tych zmiennych
			$miesiac = htmlspecialchars($_GET["miesiac"]);
			$rok = htmlspecialchars($_GET["rok"]);

			echo $pracownik -> getListObecnosci($pracownik -> getKodPracownika(), $rok, $miesiac);
			echo $pracownik -> getListNadgodziny($rok, $miesiac);
		} else {
			// Nie istnieją, więc wyświetlamy tabelkę z listą obecności z aktualnego miesiąca i roku
			echo $pracownik -> getListObecnosci($pracownik -> getKodPracownika());
			echo $pracownik -> getListNadgodziny();
		}

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>