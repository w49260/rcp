<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if (($_SESSION['rodzaj_konta'] == "Księgowa") || ($_SESSION['rodzaj_konta'] == "Szef")) {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$ksiegowa = new ksiegowa();
		$ksiegowa -> setLogin($_SESSION['login']);

		// Sprawdzamy czy wysłano zapytanie o ustawienie pensji
		if ((isset($_POST['pracownik'])) && (isset($_POST['godzinowa'])) && (isset($_POST['nadgodziny']))) {
			// Zapytanie zostało wysłane, tak więc zapisujemy nadgodziny
			$pracownik = htmlspecialchars($_POST["pracownik"]);
			$godzinowa = htmlspecialchars($_POST["godzinowa"]);
			$nadgodziny = htmlspecialchars($_POST["nadgodziny"]);

			// sprawdzamy czy stawki zostały podane
			if (($godzinowa == "") || ($nadgodziny == "")) {
				echo '<p class="center">Stawki za pensję nie zostały poprawnie wypełnione. Popraw je.</p>';
			} else {

				// rozdzielamy zmienną $pracownik, ponieważ zawiera ona niepotrzebne informacje
				$poprawionypracownik = explode(" ", $pracownik);

				if ($ksiegowa -> savePensja($poprawionypracownik[0], $godzinowa, $nadgodziny)) {
					echo '<p class="center">Stawki za pensje zostały zostały poprawnie zmienione.</p>';
				} else {
					echo '<p class="center">Stawki za pensje nie zostały poprawnie zmienione. Skontaktuj się z autorem systemu.';
				}

			}

		} else {
			// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający ustawianie pensji

			//Pokazujemy listę wszystkich pracowników
			echo '<div class="wybierzpracownika">
		Wybierz pracownika, któremu chcesz ustawić pensję:
		<br>
		<br>
		<form action="panel_ksiegowosc_ustaw_pensje.php" method="post">
	<fieldset>
	<select name="pracownik" style="width: 100%;">
		';
			// Pobieramy listę loginów pracowników
			echo $ksiegowa -> getListPracownicy(TRUE);
			echo '</select><br>
   		<br>
   		<p>Stawka godzinowa (podawana z dwoma miejscami po kropce, np. 8.50 - to 8 zł. 50 gr.): <input type="text" name="godzinowa" class="input100"></p>
   		<p>Stawka nadgodzin (podawana z dwoma miejscami po kropce, np. 8.50 - to 8 zł. 50 gr.): <input type="text" name="nadgodziny" class="input100"></p>
		<h6>*Pamiętaj, aby wypełnić powyższe pola (nawet gdy jedna ze stawek się nie zmieni)</h6>
   		<input type="submit" value="Ustaw pensję powyższemu pracownikowi" class="btn btn-success" />
   		</fieldset>
   		</form>
   		</div>';
		}

		// Wyświetlamy przycisk do wrócenia do poprzedniej strony
		echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>