<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if ($_SESSION['rodzaj_konta'] == "Szef") {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$szef = new szef();

		// Sprawdzamy czy wysłano zapytanie o wyliczenie pensji
		if ((isset($_GET['pracownik']))) {
			// Zapytanie zostało wysłane, tak więc pokazujemy kod pracownika
			$pracownik = htmlspecialchars($_GET["pracownik"]);

			echo '<h2><p class="center">Kod QR do wydrukowania dla pracownika: ' . $pracownik . '</p></h2>';

			echo $szef -> getQRPracownika($szef -> getKodPracownika($pracownik));

		} else {
			// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający dodawanie nadgodzin

			//Pokazujemy listę wszystkich pracowników
			echo '<div class="wybierzpracownika">
		Wybierz pracownika, któremu chcesz wygenerować kod QR
		<br>
		<br>
		<form action="panel_szef_generuj_qr_pracownika.php" method="get">
	<fieldset>
	<select name="pracownik" style="width: 100%;">
		';
			// Pobieramy listę loginów pracowników
			echo $szef -> getListPracownicy(FALSE);
			echo '</select><br><br>
   		<input type="submit" value="Generuj kod dla powyższego pracownika" class="btn btn-success" />
   		</fieldset>
   		</form>
   		</div>';
		}

		// Wyświetlamy przycisk do wrócenia do poprzedniej strony
		echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>