<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// sprawdzamy czy zmienna send istnieje oraz czy przesłano login i hasło
if (($_POST['send']) and ($_POST["login"]) and ($_POST["haslo"])) {
	// zapisujemy do zmiennych dane do logowania
	$login = htmlspecialchars($_POST["login"]);
	$haslo = htmlspecialchars($_POST["haslo"]);

	// tworzymy obiekt z danymi do logowania
	$pracownik = new pracownik($login, $haslo);

	// sprawdzamy czy dane do logowania zostały podane prawidłowo
	if ($pracownik -> sprawdzDane()) {
		// dane do logowania są prawidłowe, sprawdzamy rodzaj konta
		$rodzaj_konta = $pracownik -> getTypKonta();

		//Przypisujemy do sesji login oraz rodzaj konta
		$_SESSION["login"] = $login;
		$_SESSION["rodzaj_konta"] = $rodzaj_konta;

		// Wyświetlamy odpowiedni panel administracyjny dla każdego rodzaju z kont

		switch($rodzaj_konta) {
			case 'Szef' :
				header('Location: panel_szef.php');
				break;
			case 'Księgowa' :
				header('Location: panel_ksiegowa.php');
				break;
			case 'Pracownik' :
				header('Location: panel_pracownik.php');
				break;
			default :
				echo "<p class=\"center\">Nie znaleziono konfiguracji dla Twojego konta.<br><br><a href=\"index.php\" class=\"btn btn-info\">Powrót</a></p>";
				break;
		}

	} else {
		echo "<p class=\"center\">Dane do logowania są nieprawidłowe. Popraw je i spróbuj ponownie.<br><br><a href=\"index.php\" class=\"btn btn-info\">Powrót</a></p>";
	}

} else {
	// zmienne nie zostały przesłane więc błąd
	echo "<p class=\"center\">Dane do logowania nie zostały wypełnione. Popraw je i spróbuj ponownie.<br><br><a href=\"index.php\" class=\"btn btn-info\">Powrót</a></p>";
}

include ('footer.php');
?>