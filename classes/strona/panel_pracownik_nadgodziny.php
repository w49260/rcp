<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Panel ten może przeglądać każdy, więc nie sprawdzamy rodzaju konta
	//if ($_SESSION['rodzaj_konta'] == "Pracownik") {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$pracownik = new pracownik();
		$pracownik -> setLogin($_SESSION['login']);
		
		// Sprawdzamy czy wysłano zapytanie o dodanie nadgodzin
		if ((isset($_POST['data'])) && (isset($_POST['czas'])) && (isset($_POST['dodatkoweinformacje']))) {
			// Zapytanie zostało wysłane, tak więc zapisujemy nadgodziny
			$data = htmlspecialchars($_POST["data"]);
			$czas = htmlspecialchars($_POST["czas"]);
			$dodatkowe_informacje = htmlspecialchars($_POST["dodatkoweinformacje"]);
			
			if ($pracownik->saveNadgodziny($data, $czas, $dodatkowe_informacje))
			{
				echo '<p class="center">Nadgodziny zostały dodane poprawnie.</p>';
			} else {
				echo '<p class="center">Nadgodziny nie zostały poprawnie dodane. Skontaktuj się z autorem systemu.';
			}
			
		} else {
			// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający dodawanie nadgodzin
			echo "<p class=\"center\"><strong>Dodawanie nadgodzin pracownika: " . $pracownik -> getImie() . " " . $pracownik -> getNazwisko() . "</strong></p>";
			// Dodajemy potrzebne skrypty do wyświetlenia kalendarza
			echo '	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
				<script src="//code.jquery.com/jquery-1.10.2.js"></script>
				<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	 			<script>
				$(function() {
				$( "#datepicker" ).datepicker({
					dateFormat: "yy-mm-dd"
				});
				});
				</script>	';

			//Wyświetlamy formularz
			echo '	<br>
				<form action="panel_pracownik_nadgodziny.php" method="post">
				<div class="nadgodziny">
				<p>Data: <input type="text" id="datepicker" name="data" class="input100"></p>
				<p>Czas w godz. (podawany z dwoma miejscami po kropce, np. 1.50 - to 1 godz. 30 min.): <input type="text" name="czas" class="input100"></p>
				<p>Dodatkowe informacje (np. co robiłeś): <input type="text" name="dodatkoweinformacje" class="input100"></p>
				<input type="submit" value="Dodaj nadgodziny" class="btn btn-success btn-lg btn-block" />
				</form>';
		}

		// Wyświetlamy przycisk do wrócenia do poprzedniej strony
		echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	//} else {
	//	echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	//}
}

// Dodajemy własną stopkę, gdyż w aktualnej jest już dodane jquery a on się "gryzie" z jquery z kalendarza
//include ('footer.php');
echo '</div>
<p class="footer">Autorzy systemu: Mateusz Kucab, Władysław Majdanik, Marcin Kula, Hubert Grobelny</p>
 </div>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>';
?>