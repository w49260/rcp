<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Sprawdzamy czy użytkownik może przeglądać ten panel
	if (($_SESSION['rodzaj_konta'] == "Księgowa") || ($_SESSION['rodzaj_konta'] == "Szef")) {
		// Rodzaj konta zgadza się, a więc wyświetlamy panel

		$ksiegowa = new ksiegowa();
		$ksiegowa -> setLogin($_SESSION['login']);

		// Sprawdzamy czy wysłano zapytanie o wyliczenie pensji
		if ((isset($_GET['pracownik'])) && (isset($_GET['miesiac'])) && (isset($_GET['rok']))) {
			// Zapytanie zostało wysłane, tak więc zapisujemy nadgodziny
			$pracownik = htmlspecialchars($_GET["pracownik"]);
			$miesiac = htmlspecialchars($_GET["miesiac"]);
			$rok = htmlspecialchars($_GET["rok"]);

			// Pokazujemy raport obecności

			echo "<p class=\"center\">Raport dla pracownika: <strong>" . $pracownik . '</strong></p>';

			echo $ksiegowa -> getListObecnosci($ksiegowa -> getKodPracownika($pracownik));

		} else {
			// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający dodawanie nadgodzin

			//Pokazujemy listę wszystkich pracowników
			echo '<div class="wybierzpracownika">
		Wybierz pracownika, któremu chcesz wygenerować raport obecności:
		<br>
		<br>
		<form action="panel_ksiegowosc_generuj_raport_obecnosci.php" method="get">
	<fieldset>
	<select name="pracownik" style="width: 100%;">
		';
			// Pobieramy listę loginów pracowników
			echo $ksiegowa -> getListPracownicy(FALSE);
			echo '</select><br><br>
		Podaj miesiąc i rok:
		<br>
		<select name="miesiac">
			<option>01</option>
			<option>02</option>
			<option>03</option>
			<option>04</option>
			<option>05</option>
			<option>06</option>
			<option>07</option>
			<option>08</option>
			<option>09</option>
			<option>10</option>
			<option>11</option>
			<option>12</option>
   		</select>
   		<select name="rok">
			<option>2014</option>
			<option>2015</option>
			<option>2016</option>
   		</select>
   		<br>
   		<br>
   		<input type="submit" value="Generuj raport dla powyższego pracownika" class="btn btn-success" />
   		</fieldset>
   		</form>
   		</div>';
		}

		// Wyświetlamy przycisk do wrócenia do poprzedniej strony
		echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	} else {
		echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	}
}

include ('footer.php');
?>