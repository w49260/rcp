<?php
session_start();
// rozpoczęcie sesji

include ('header.php');

// Sprawdzamy czy użytkownik jest zalogowany
if (!isset($_SESSION['login'])) {
	echo "<p class=\"center\">Nie jesteś zalogowany.<br><br><a href=\"index.php\" class=\"btn btn-info\">Zaloguj się ponownie.</a></p>";
} else {
	// Panel ten może przeglądać każdy, więc nie sprawdzamy rodzaju konta
	//if ($_SESSION['rodzaj_konta'] == "Pracownik") {
	// Rodzaj konta zgadza się, a więc wyświetlamy panel

	$pracownik = new pracownik();
	$pracownik -> setLogin($_SESSION['login']);

	// Sprawdzamy czy wysłano zapytanie o wyliczenie pensji
	if ((isset($_POST['haslo']))) {
		// Zapytanie zostało wysłane, tak więc pokazujemy kod pracownika
		$haslo = $_POST["haslo"];

		if ($pracownik -> zmienHaslo($haslo)) {
			echo '<h2><p class="center">Twoje hasło zostało poprawnie zmienione.</p></h2>';
		} else {
			echo '<h2><p class="center">Wystąpił błąd podczas zmiany hasła. Skontaktuj się z autorem systemu.</p></h2>';
		}

	} else {
		// Zapytania nie było, tak więc wyświetlamy formularz umożliwiający zmianę hasła

		echo '
		<form action="panel_pracownik_zmien_haslo.php" method="post">
			<p class="center">Podaj nowe hasło: <input type="password" name="haslo" ></p>
			<input type="submit" value="Zmień hasło" class="btn btn-success btn-lg btn-block" />
		</form>';
	}

	// Wyświetlamy przycisk do wrócenia do poprzedniej strony
	echo '<br><a href="javascript:history.back()" type="button" class="btn btn-info btn-lg btn-block" style="font-size: 13px;">Wróć</a>';

	//} else {
	//	echo "<p class=\"center\">Nie masz prawa do przeglądania tej strony.<br><br><a href=\"index.php\" class=\"btn btn-info\">Wróć do logowania.</a></p>";
	//}
}

include ('footer.php');
?>