<?php

/**
 * Klasa pracownik
 */
class pracownik {

	private $login;
	private $haslo;
	private $db;

	/**
	 * Konstruktor klasy pracownik
	 */
	public function __construct($login = NULL, $haslo = NULL) {
		$this -> login = $login;
		$this -> haslo = md5($haslo);

		// wywołujemy w konstruktorze połączenie z bazą
		$this -> db = new db();
	}

	/**
	 * Funkcja do zapisywania loginu
	 */
	public function setLogin($login) {
		$this -> login = $login;
	}

	/**
	 * Funkcja sprawdzająca dane do logowania użytkownika
	 * zwraca TRUE w przypadku gdy są prawidłowe, FALSE jeśli nie
	 */
	public function sprawdzDane() {
		$zapytanie = $this -> db -> select("SELECT * FROM pracownicy WHERE login='$this->login' and haslo='$this->haslo'");
		// sprawdzamy czy w bazie znaleźliśmy danego użytkownika
		if (mysql_num_rows($zapytanie) == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Funkcja zwraca imię użytkownika
	 */
	public function getImie() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT imie FROM pracownicy WHERE login='$this->login'"));

		$imie = $zapytanie["imie"];
		return $imie;
	}

	/**
	 * Funkcja zwraca nazwisko użytkownika
	 */
	public function getNazwisko() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT nazwisko FROM pracownicy WHERE login='$this->login'"));

		$nazwisko = $zapytanie["nazwisko"];
		return $nazwisko;
	}

	/**
	 * Funkcja zwraca rodzaj konta użytkownika
	 * Podstawowe konta użytkowników: Szef, Pracownik, Księgowa
	 */
	public function getTypKonta() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT rodzaj_konta.nazwa FROM pracownicy, rodzaj_konta WHERE pracownicy.login='$this->login' and rodzaj_konta.id=pracownicy.rodzaj_konta_id"));

		$typ_konta = $zapytanie["nazwa"];
		return $typ_konta;
	}

	/**
	 * Funkcja pozwalająca zmienić hasło do konta
	 * parametr $nowehaslo - nowe hasło
	 */
	public function zmienHaslo($nowehaslo) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		// liczmy md5 z nowego hasła
		$nowehaslo = md5($nowehaslo);

		$zapytanie = $this -> db -> query("UPDATE `pracownicy` SET `haslo` = '$nowehaslo' WHERE `login` = '$this->login';");
		// sprawdzamy czy zapytanie wykonało się dobrze
		if ($zapytanie) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Funkcja zwraca kod pracownika
	 */
	public function getKodPracownika() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT kod_pracownika FROM pracownicy WHERE login='$this->login'"));

		$kodpracownika = $zapytanie["kod_pracownika"];
		return $kodpracownika;
	}

	/**
	 * Funkcja zwraca stawke godzinową pracownika
	 */
	public function getStawkaGodzinowa() {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}
		$zapytanie = mysql_fetch_array($this -> db -> select("SELECT stawka_godzinowa FROM pracownicy WHERE login='$this->login'"));

		$stawkagodzinowa = $zapytanie["stawka_godzinowa"];
		return $stawkagodzinowa;
	}

	/**
	 * Funcja zwraca tabelkę z listą obecnosci
	 * parametr $kodpracownika - to kod pracownika (można go wywołać funkcją getKodPracownika() )
	 * parametr rok - to rok w ktorym ma byc lista obecnosci - domyslnie aktualny rok
	 * parametr miesiac - to miesiac w ktorym ma byc lista obecnosci - domyslnie aktualny miesiac
	 */
	public function getListObecnosci($kodpracownika, $rok = NULL, $miesiac = NULL) {
		// Sprawdzamy czy parametry rok i miesiac sa puste, jesli tak to domyslnie ustawiamy na aktualny rok i miesiac
		if (is_null($rok)) {
			$rok = date("Y");
		}
		if (is_null($miesiac)) {
			$miesiac = date("m");
		}

		// wyświetlamy kod tabelki
		echo '<div class="tabelaobecnosci">
		<table class="table" style=" display: inline;">
				<caption style="font-size: 16px; color: #3A87AD;">
					Lista obecności w pracy (' . $rok . '-' . $miesiac . ')
				</caption>
				<thead>
					<tr>
						<th>Lp</th>
						<th>Data</th>
						<th>Status w pracy</th>
						<th>Przepracowany czas</th>
					</tr>
				</thead>
				<tbody>';

		// zapisujemy wszystkie dane do zmiennej
		$zapytanie = $this -> db -> select("SELECT data, czy_wyjscie FROM odklikania WHERE kod_pracownika='$kodpracownika' and month(data)=month('$rok-$miesiac-01')");

		$lp = 0;
		$wejscie = null;
		// pętlą wyświetlamy wszystkie dane
		while ($dane = mysql_fetch_array($zapytanie)) {
			$lp++;

			// Sprawdzamy czy użytkownik wszedł czy wyszedł - i w zależności od tego wyświetlamy inny kolor w tabeli oraz wyświetlamy przepracowany czas
			if ($dane["czy_wyjscie"]) {
				// użytkownik wyszedł
				echo '<tr class="danger">';
				echo '<td>' . $lp . '</td>';
				echo '<td>' . $dane["data"] . '</td>';
				echo '<td>Wyjście</td>';

				// użytkownik wyszedł więc liczymy przepracowany czas
				$przepracowanyczas = strtotime($dane["data"]) - strtotime($wejscie);
				echo '<td>' . date("G", $przepracowanyczas - 3600) . " godz. " . date("i", $przepracowanyczas) . " min. " . date("s", $przepracowanyczas) . ' sec.</td>';
				// $przepracowanyczas-3600 ponieważ php dodaje zawsze + 1 godz
			} else {
				echo '<tr class="success">';
				echo '<td>' . $lp . '</td>';
				echo '<td>' . $dane["data"] . '</td>';
				echo '<td>Wejście</td>';
				echo '<td></td>';

				// zapisujemy do zmiennej date wejścia
				$wejscie = $dane["data"];
			}
		}
		// wyświetlamy końcowy kod tabelki
		echo '</tr>
			</tbody>
			</table></div>';
	}

	/**
	 * Funcja zwraca tabelkę z listą nadgodzin
	 * parametr rok - to rok w ktorym ma byc lista nadgodzin - domyslnie aktualny rok
	 * parametr miesiac - to miesiac w ktorym ma byc lista nadgodzin - domyslnie aktualny miesiac
	 */
	public function getListNadgodziny($rok = NULL, $miesiac = NULL) {
		// Sprawdzamy czy parametry rok i miesiac sa puste, jesli tak to domyslnie ustawiamy na aktualny rok i miesiac
		if (is_null($rok)) {
			$rok = date("Y");
		}
		if (is_null($miesiac)) {
			$miesiac = date("m");
		}

		// wyświetlamy kod tabelki
		echo '<div class="tabelaobecnosci">
		<table class="table" style=" display: inline;">
				<caption style="font-size: 16px; color: #3A87AD;">
					Lista nadgodzin (' . $rok . '-' . $miesiac . ')
				</caption>
				<thead>
					<tr>
						<th>Lp</th>
						<th>Data</th>
						<th>Czas</th>
						<th>Dodatkowe informacje</th>
					</tr>
				</thead>
				<tbody>';

		// zapisujemy wszystkie dane do zmiennej
		$zapytanie = $this -> db -> select("SELECT data, czas, dodatkowe_informacje FROM nadgodziny WHERE login='$this->login' and month(data)=month('$rok-$miesiac-01')");

		$lp = 0;
		$wejscie = null;
		// pętlą wyświetlamy wszystkie dane
		while ($dane = mysql_fetch_array($zapytanie)) {
			$lp++;

			// Usuwamy z daty niepotrzebną godzinę
			$data = str_replace(' 00:00:00', '', $dane["data"]);

			echo '<tr class="success">';
			echo '<td>' . $lp . '</td>';
			echo '<td>' . $data . '</td>';
			echo '<td>' . $dane["czas"] . '</td>';
			echo '<td>' . $dane["dodatkowe_informacje"] . '</td>';
			echo '<td></td>';

		}
		// wyświetlamy końcowy kod tabelki
		echo '</tr>
			</tbody>
			</table></div>';
	}

	/**
	 * Funkcja służąca do zapisywania nadgodzin
	 * parametr $data to data wykonywania dodatkowych nadgodzin w formacie 2014-06-03
	 * parametr $czas to ilość czasu wykonania pracy w formacie 1.50
	 * parametr $dodatkowe_informacje to string o dodatkowych informacjach wykonywanych nadgodzin
	 */
	public function saveNadgodziny($data, $czas, $dodatkowe_informacje) {
		// sprawdzamy czy obiekt istnieje (może nie istnieć w przypadku dziedziczenia - podczas dziedziczenia konstruktor nie jest dziedziczony!)
		if ($this -> db == null) {
			$this -> db = new db();
		}

		$zapytanie = $this -> db -> query("INSERT INTO `nadgodziny` (`id`, `login`, `data`, `czas`, `dodatkowe_informacje`) VALUES (NULL, '$this->login', '$data', '$czas', '$dodatkowe_informacje');");
		// sprawdzamy czy zapytanie wykonało się dobrze
		if ($zapytanie) {
			return true;
		} else {
			return false;
		}
	}

}
?>

