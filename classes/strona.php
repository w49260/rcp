<?php

/**
 * Klasa strona - do wyświetlenia strony html
 */
class strona {
	
	private $tytulstrony;

	public function __construct($tytulstrony) {
		$this -> tytulstrony = $tytulstrony;
	}

	public function wyswietl($czesc_html) {
		$tytulstrony = $this->tytulstrony;
		include ('strona/'.$czesc_html.'.php');


	}

	public function __destruct() {
		
	}
}
?>
