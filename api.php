<?php
// sprawdzamy czy jest zmienna kod_pracownika
if (empty($_GET["kod_pracownika"])) {
	// nie ma więc, błąd
	echo 'error';
} else {
	// zapisujemy kod_pracownika do zmiennej
	$kod_pracownika = htmlspecialchars($_GET["kod_pracownika"]);

	// wczytujemy klasy
	require_once 'config/loader.php';

	// wywołujemy klasę odklikiwanie
	$odklik = new odklikiwanie($kod_pracownika);

	// sprawdzamy czy taki pracownik istnieje w bazie
	if ($odklik -> czyPracownikIstnieje()) {
		// pracownik istnieje, sprawdzmy czy się kiedyś odklikiwał
		if ($odklik -> czyPracownikSieOdklikiwal()) {
			// pracownik się odklikiwał

			// sprawdzamy czy jego ostatnie odklikiwanie nie trwało krócej niż 60 sekund, bo jeśli tak to wyświetlamy komunikat, że za szybko się odklikuje

			if ($odklik -> ileMineloOdOstatniegoOdklikiwania() < 60) {
				// roznica w odklikaniu jest MNIEJSZA niz 60 sekund tak więc wyswietlamy komunikat
				echo "Za szybko sie odklikujesz (ponizej 60 sekund). Odczekaj chwile.";
			} else {
				// roznica jest wieksza niz 60 sekund tak wiec dodajemy kolejny odklik

				// sprawdzamy czy jego ostatnia akcja to było wyjście (bo jeśli tak, to trzeba to zaznaczyć w bazie)
				if ($odklik -> czyWyjscie()) {
					// pracownik wychodził więc odklikaj przyjście
					$odklik -> odklikajPrzyjscie(false);
				} else {
					// pracownik przyszedł już więc odklikaj wyjście
					$odklik -> odklikajPrzyjscie(true);
				}
			}
		} else {
			// pracownik się nie odklikiwał, a wiec odklikujemy przyjście
			$odklik -> odklikajPrzyjscie(false);
		}
	} else {
		echo "Nie pracujesz u nas";
	}
}
?>